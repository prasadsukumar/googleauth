import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:googleauth/countries.dart';
import 'package:googleauth/Background.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  bool _isLoggedIn = false;

  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _login() async {
    try {
      await _googleSignIn.signIn();
      setState(() {
        _isLoggedIn = true;
      });
    } catch (err) {
      print(err);
    }
  }

  _logout() {
    _googleSignIn.signOut();
    setState(() {
      _isLoggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: _isLoggedIn
              ? Container(
                  alignment: Alignment.center,
                  color: Colors.blueGrey.shade900,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Logged In',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 8),
                      _googleSignIn.currentUser.photoUrl == null
                          ? CircleAvatar(
                              maxRadius: 25,
                              backgroundImage: NetworkImage(
                                  'https://images.search.yahoo.com/images/view;_ylt=Awr9DWuOfG5gan4ARcCJzbkF;_ylu=c2VjA3NyBHNsawNpbWcEb2lkAzY5MTExYTMzMzYwNzkwM2UxOTZmMjE2YzVhOGIyMWI1BGdwb3MDNQRpdANiaW5n?back=https%3A%2F%2Fimages.search.yahoo.com%2Fsearch%2Fimages%3Fp%3Dgoogle%2Bempty%2Bdp%2Bpic%26n%3D60%26ei%3DUTF-8%26fr%3Dmcafee%26fr2%3Dsb-top-images.search%26tab%3Dorganic%26ri%3D5&w=256&h=256&imgurl=c-sf.smule.com%2Fz0%2Faccount%2Ficon%2Fv4_defpic.png&rurl=http%3A%2F%2Fwww.smule.com%2Fsmartchick8&size=+6.7KB&p=google+empty+dp+pic&oid=69111a333607903e196f216c5a8b21b5&fr2=sb-top-images.search&fr=mcafee&tt=smartchick8%26%2339%3Bs+Profile+%7C+Smule&b=0&ni=120&no=5&ts=&tab=organic&sigr=2Nk6ln_l8sc2&sigb=0esCp4g7lqSc&sigi=CaDrE8PW1Ue2&sigt=p33AMGlqIutc&.crumb=qqd2tcO5ayL&fr=mcafee&fr2=sb-top-images.search'),
                            )
                          : CircleAvatar(
                              maxRadius: 25,
                              backgroundImage: NetworkImage(
                                  _googleSignIn.currentUser.photoUrl),
                            ),
                      SizedBox(height: 8),
                      Text(
                        'Name: ' + _googleSignIn.currentUser.displayName,
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 8),
                      Text(
                        'Email: ' + _googleSignIn.currentUser.email,
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 8),
                      ElevatedButton(
                        onPressed: () {
                          _logout();
                        },
                        child: Text('Logout'),
                      ),
                      SizedBox(height: 8),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new Country()));
                        },
                        child: Text('Get countries'),
                      )
                    ],
                  ),
                )
              : Container(
                  constraints: BoxConstraints.expand(),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("images/design1.jpg"),
                          fit: BoxFit.cover)),
                  child: Column(
                    children: [
                      Spacer(),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              width: 175,
                              child: Text(
                                'Welcome to Nexware',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                width: 175,
                                child: Text(
                                  'Task done by Prasad S',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        padding: EdgeInsets.all(4),
                        child: OutlineButton.icon(
                          label: Text(
                            'Sign In With Google',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          shape: StadiumBorder(),
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          highlightedBorderColor: Colors.white,
                          borderSide: BorderSide(color: Colors.white),
                          textColor: Colors.white,
                          icon: FaIcon(FontAwesomeIcons.google,
                              color: Colors.red),
                          onPressed: () {
                            _login();
                          },
                        ),
                      ),
                      SizedBox(height: 12),
                      Text(
                        'Login to continue',
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      ),
                      Spacer(),
                    ],
                  )),
        ),
      ),
    );
  }
}
